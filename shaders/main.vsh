; $MODE = "UniformRegister"
; $UNIFORM_VARS[0].name = "uMVP"
; $UNIFORM_VARS[0].type = "Matrix4x4"
; $UNIFORM_VARS[0].count = 1
; $UNIFORM_VARS[0].offset = 0
; $UNIFORM_VARS[0].block = -1
; $UNIFORM_VARS[1].name = "uLightModelPosition"
; $UNIFORM_VARS[1].type = "Matrix4x4"
; $UNIFORM_VARS[1].count = 1
; $UNIFORM_VARS[1].offset = 16
; $UNIFORM_VARS[1].block = -1
; $ATTRIB_VARS[0].name = "aColour"
; $ATTRIB_VARS[0].type = "Float3"
; $ATTRIB_VARS[0].location = 0
; $ATTRIB_VARS[1].name = "aPosition"
; $ATTRIB_VARS[1].type = "Float4"
; $ATTRIB_VARS[1].location = 1
; $ATTRIB_VARS[2].name = "aVertexNormal"
; $ATTRIB_VARS[2].type = "Float4"
; $ATTRIB_VARS[2].location = 2

; Note to self: these are packed in groups of four here, and *not* packed on
; the PS side. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
; Semantic 0: aColour
; Semantic 1: aVertexNormal
; Semantic 2: vtx - uLightModelPosition
; $SPI_VS_OUT_CONFIG.VS_EXPORT_COUNT = 2
; $NUM_SPI_VS_OUT_ID = 1
; $SPI_VS_OUT_ID[0].SEMANTIC_0 = 0
; $SPI_VS_OUT_ID[0].SEMANTIC_1 = 1
; $SPI_VS_OUT_ID[0].SEMANTIC_2 = 2

; R0 is sacrificed as temporary
; MVP is in C0,1,2,3

;   Time to multiply out the uMVP matrix (stored in column-major order)
:   Input matrix:
;   | C0.x C1.x C2.x C3.x |
;   | C0.y C1.y C2.y C3.y |
;   | C0.z C1.z C2.z C3.z |
;   | C0.w C1.w C2.w C3.w |
;   The Plan: (xywz are in R2)
;   PV.x | C0.x*x + C1.x*y + C2.x*z + C3.x*w |
;   PV.y | C0.y*x + C1.y*y + C2.y*z + C3.y*w |
;   PV.z | C0.z*x + C1.z*y + C2.z*z + C3.z*w |
;   PV.w | C0.w*x + C1.w*y + C2.w*z + C3.w*w |
;   This happens for pc 01:0-3.

;   We also need to calculate a vector between this vertex and
;   uLightModelPosition - this is easy, just subtract each component
;   That is: PARAM1 = R2 - C4.
;   This happens for pc 01:4. Normalising happens in the pixel shader b/c lazy.

00 CALL_FS NO_BARRIER
01 ALU: ADDR(32) CNT(28)
    0 x: MUL ____, R2.x, C0.x
      y: MUL ____, R2.x, C0.y
      z: MUL ____, R2.x, C0.z
      w: MUL ____, R2.x, C0.w
    1 x: MULADD R0.x, R2.y, C1.x, PV0.x
      y: MULADD R0.y, R2.y, C1.y, PV0.y
      z: MULADD R0.z, R2.y, C1.z, PV0.z
      w: MULADD R0.w, R2.y, C1.w, PV0.w
    2 x: MULADD R0.x, R2.z, C2.x, PV0.x
      y: MULADD R0.y, R2.z, C2.y, PV0.y
      z: MULADD R0.z, R2.z, C2.z, PV0.z
      w: MULADD R0.w, R2.z, C2.w, PV0.w
    3 x: MULADD R6.x, R2.w, C3.x, PV0.x
      y: MULADD R6.y, R2.w, C3.y, PV0.y
      z: MULADD R6.z, R2.w, C3.z, PV0.z
      w: MULADD R6.w, R2.w, C3.w, PV0.w
    4 x: ADD R0.x, -R2.x, C4.x
      y: ADD R0.y, -R2.y, C4.y
      z: ADD R0.z, -R2.z, C4.z
    5 x: DOT4_e ____, R3.x, R3.x
      y: DOT4_e ____, R3.y, R3.y
      z: DOT4_e ____, R3.z, R3.z
      w: DOT4_e ____, R3.w, R3.w
    6 t: RSQ_e  ____, PV0.x
    7 x: MUL R3.x, R3.x, PS0
      y: MUL R3.y, R3.y, PS0
      z: MUL R3.z, R3.z, PS0
      w: MUL R3.w, R3.w, PS0
02 EXP: PARAM0, R1.xyzw
03 EXP: PARAM1, R3.xyzw
04 EXP_DONE: PARAM2, R0.xyz1
05 EXP_DONE: POS0, R6.xyzw
END_OF_PROGRAM
