#pragma once
#include <gx2/shaders.h>
#include <glm/glm.hpp>
#include "effects/Distort.hpp"

class DistortParams {
public:
    float distortAmp;
    float distortAmpSpeed;
    glm::vec2 distort;
    glm::vec2 distortSpeed;
    glm::vec2 chromaGShift;
    glm::vec2 chromaGShiftSpeed;
    glm::vec2 chromaBShift;
    glm::vec2 chromaBShiftSpeed;

    void animate(float delta_s) {
        distortAmp += distortAmpSpeed * delta_s;
        distort += distortSpeed * delta_s;
        chromaGShift += chromaGShiftSpeed * delta_s;
        chromaBShift += chromaBShiftSpeed * delta_s;
    }

    void applyTo(Distort& pass) {
        pass.distortAmplitude(distortAmp);
        pass.distortParams(distort);
        pass.chromaBShift(chromaBShift);
        pass.chromaGShift(chromaGShift);
    }
};

class GfxState {
public:
    glm::mat4 ProjectionMatrix;
    glm::mat4 ViewMatrix;

    GX2UniformVar* uMVP;
    GX2UniformVar* uLightModelPosition;

    uint32_t aPosition;
    uint32_t aColour;
    uint32_t aVertexNormal;

    glm::vec4 LightPosition {0.0f, 0.0f, -3.0f, 1.0f};

    glm::vec3 camPosition {0.0f, 0.0f, -5.0f};

    bool black = false;
    float zoom2d = 0.0f;

    struct {
        Distort pass;
        DistortParams params;
    } donut;
    struct {
        Distort pass;
        DistortParams params;
    } tvIntro;

/*  Make sure this is safe to use - not neccesarily valid */
    bool check() const {
        return (uMVP != NULL) && (uLightModelPosition != NULL) && donut.pass.valid && tvIntro.pass.valid;
    }
};
