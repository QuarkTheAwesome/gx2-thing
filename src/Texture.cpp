#include "Texture.hpp"
#include <iostream>
#include <malloc.h>

#include <gx2/context.h>
#include <gx2/surface.h>
#include <gx2/event.h>
#include <gx2/state.h>
#include <gx2/shaders.h>
#include <gx2/texture.h>
#include <gx2/mem.h>
#include <gx2/sampler.h>
#include <gx2/draw.h>
#include <gx2/registers.h>
#include <gx2/clear.h>
#include <gx2r/surface.h>
#include <gx2r/buffer.h>
#include <gx2r/draw.h>
#include <whb/gfx.h>

#include <glm/glm.hpp>

#include "gx2util.hpp"
#include "shaders.h"

Texture::Texture(int width, int height) : width(width), height(height) {
    if (!WHBGfxLoadGFDShaderGroup(&shader, 0, texture_shader)) {
        printf("Failed to parse texture shader!\n");
        return;
    }

    uint32_t buffer = 0;
    aPosition = buffer++;
    WHBGfxInitShaderAttribute(&shader,
        "aPosition", aPosition, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32);
    aTexCoord = buffer++;
    WHBGfxInitShaderAttribute(&shader,
        "aTexCoord", aTexCoord, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32);

    WHBGfxInitFetchShader(&shader);

    GX2InitSampler(&sampler,
        GX2_TEX_CLAMP_MODE_WRAP, GX2_TEX_XY_FILTER_MODE_POINT
    );

    tex.surface.width = this->width;
    tex.surface.height = this->height;
    GX2CalcSurfaceSizeAndAlignment(&tex.surface);
    GX2InitTextureRegs(&tex);
    tex.surface.image = memalign(tex.surface.alignment, tex.surface.imageSize);
    if (!tex.surface.image) return;

    for (uint i = 0; i < (tex.surface.imageSize / sizeof(int)); i++) {
        ((int*)tex.surface.image)[i] = rand();
    }
    GX2Invalidate(GX2_INVALIDATE_MODE_CPU_TEXTURE, tex.surface.image, tex.surface.imageSize);

    GX2RCreateBuffer(&aPositionBuffer);
    GX2RCreateBuffer(&aTexCoordBuffer);
    DrawCoords* aPositions = (DrawCoords*)GX2RLockBufferEx(
        &aPositionBuffer, (GX2RResourceFlags)0
    );
    DrawCoords* aTexCoords = (DrawCoords*)GX2RLockBufferEx(
        &aTexCoordBuffer, (GX2RResourceFlags)0
    );
    OnLeavingScope r([&] {
        GX2RUnlockBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
        GX2RUnlockBufferEx(&aTexCoordBuffer, (GX2RResourceFlags)0);
    });

#ifndef NDEBUG
    if (GX2RSizeof(aPositionBuffer) != sizeof(*aPositions) ||
        GX2RSizeof(aTexCoordBuffer) != sizeof(*aTexCoords)
    ) {
        std::cerr << __FILE__ << ":" << __LINE__ << ": GX2R buffer size mismatch: "
            << GX2RSizeof(aPositionBuffer) << " != " << sizeof(*aPositions) << " || "
            << GX2RSizeof(aTexCoordBuffer) << " != " << sizeof(*aTexCoords) << "\n";
        return;
    }
#endif

    *aPositions = (DrawCoords) { .coords = {
        [0] = glm::vec2(-1.0f,  1.0f),
        [1] = glm::vec2( 1.0f,  1.0f),
        [2] = glm::vec2( 1.0f, -1.0f),
        [3] = glm::vec2(-1.0f, -1.0f),
    }};
    *aTexCoords = (DrawCoords) { .coords = {
        [0] = glm::vec2( 0.0f,  0.0f),
        [1] = glm::vec2( 1.0f,  0.0f),
        [2] = glm::vec2( 1.0f,  1.0f),
        [3] = glm::vec2( 0.0f,  1.0f),
    }};

    valid = true;
}

void Texture::Load(std::basic_istream<char>& stream) {
    if (!tex.surface.image) {
        std::cout << "tex: null img\n";
        return;
    }
    char* image = (char*)tex.surface.image;
    for (size_t offset = 0; offset < tex.surface.imageSize; offset += tex.surface.pitch) {
        stream.read(image, tex.surface.pitch);
        image += tex.surface.pitch;
    }
    GX2Invalidate(GX2_INVALIDATE_MODE_CPU_TEXTURE, tex.surface.image, tex.surface.imageSize);
}

Texture::~Texture() {
    GX2RDestroyBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
    GX2RDestroyBufferEx(&aTexCoordBuffer, (GX2RResourceFlags)0);
    if (tex.surface.image) free(tex.surface.image);
}

void Texture::SetDrawCoords(DrawCoords coords) {
    DrawCoords* aPositions = (DrawCoords*)GX2RLockBufferEx(
        &aPositionBuffer, (GX2RResourceFlags)0
    );
    OnLeavingScope r([&] {
        GX2RUnlockBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
    });

#ifndef NDEBUG
    if (GX2RSizeof(aPositionBuffer) != sizeof(*aPositions)) {
        std::cerr << __FILE__ << ":" << __LINE__ << ": GX2R buffer size mismatch: "
            << GX2RSizeof(aPositionBuffer) << " != " << sizeof(*aPositions) << "\n";
        return;
    }
#endif

    *aPositions = coords;
}

void Texture::Draw() {
/*  Disable blending - this particular demo doesn't need it,
 *  but later projects certainly could */
    GX2SetColorControl(GX2_LOGIC_OP_COPY, 0x00, FALSE, TRUE);
/*  Disable depth test */
    GX2SetDepthOnlyControl(FALSE, FALSE, GX2_COMPARE_FUNC_ALWAYS);

    GX2SetFetchShader(&shader.fetchShader);
    GX2SetVertexShader(shader.vertexShader);
    GX2SetPixelShader(shader.pixelShader);

    GX2SetPixelSampler(&sampler, 0);
    GX2SetPixelTexture(&tex, 0);

    GX2RSetAttributeBuffer(
        &aPositionBuffer, aPosition,
        aPositionBuffer.elemSize, 0
    );
    GX2RSetAttributeBuffer(
        &aTexCoordBuffer, aTexCoord,
        aTexCoordBuffer.elemSize, 0
    );

    GX2DrawEx(GX2_PRIMITIVE_MODE_QUADS,
        aPositionBuffer.elemCount, 0, 1);
}
