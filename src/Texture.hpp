#pragma once
#include <iostream>
#include <gx2/surface.h>
#include <gx2/sampler.h>
#include <gx2/texture.h>
#include <gx2r/buffer.h>
#include <gx2r/resource.h>
#include <whb/gfx.h>

#include <glm/glm.hpp>

#include "util.h"

class Texture {
public:
    Texture(int width, int height);
    ~Texture();

    bool valid = false;

    void Draw();
    void SetDrawCoords(DrawCoords coords);
    void Load(std::basic_istream<char>& stream);

    //cba to implement these
    Texture(Texture const &) = delete;
    void operator=(Texture const &x) = delete;

    WHBGfxShaderGroup shader;
    uint32_t aPosition;
    uint32_t aTexCoord;
private:
    GX2RBuffer aPositionBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec2),
        .elemCount = 4,
    };
    GX2RBuffer aTexCoordBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec2),
        .elemCount = 4,
    };
    GX2Sampler sampler;

    GX2Texture tex = {
        .surface = (GX2Surface) {
            .dim = GX2_SURFACE_DIM_TEXTURE_2D,
            .depth = 1,
            .mipLevels = 1,
            .format = GX2_SURFACE_FORMAT_UNORM_R8_G8_B8_A8,
            .use = GX2_SURFACE_USE_TEXTURE,
            .tileMode = GX2_TILE_MODE_LINEAR_ALIGNED,
        },
        .viewNumSlices = 1,
        .compMap = 0x00010203,
    };

    int width, height;
};
