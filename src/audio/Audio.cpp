#include "Audio.hpp"

#include <sndcore2/core.h>
#include <sndcore2/voice.h>
#include <coreinit/cache.h>
#include <cstdlib>
#include <cstdint>

namespace Audio {

static const int NumChannels = 2;
static std::array<AXVoice*, NumChannels> voices;

static const float SampleRate = 48000.0f;
static const AXVoiceFormat SampleFormat = AX_VOICE_FORMAT_LPCM16;
static const size_t SampleLen = (SampleFormat == AX_VOICE_FORMAT_LPCM16) ? 2 : 1;
static const int SampleBufferLen = 144 * 4 * NumChannels;
static const int SampleBufferSize = SampleBufferLen * SampleLen;
static const int NumBuffers = 2;

uint8_t* samples;
uint16_t* l_samples;
uint16_t* r_samples;

void InitAudio(OggDecoder& decoder) {
    AXInitParams initparams = {
        .renderer = AX_INIT_RENDERER_48KHZ,
        .pipeline = AX_INIT_PIPELINE_SINGLE,
    };
    AXInitWithParams(&initparams);

    AXVoiceVeData vol = {
        .volume = 0x8000,
    };

    AXVoiceDeviceMixData stereo_mix[2][6] = {
        { //left
            { //left chan
                .bus = { { .volume = 0x8000 } },
            },
            {}, //right chan
        },
        { //right
            {}, //left chan
            { //right chan
                .bus = { { .volume = 0x8000 } },
            },
        },
    };

    const long size = decoder.Size();
    samples = (uint8_t*)malloc(size);
    printf("decoding ogg\n");
    for (long off = 0; off < size;) {
        long ret = decoder.Read(samples + off, size - off);
        if (ret < 0) {
            if (size - off < 144) {
                //eh, good enough
                break;
            }
            printf("decode error! %ld (%ld %ld %p)\n", ret, size, size - off, samples);
            return;
        } else if (ret == 0) {
            printf("eof! @ %ld!\n", off);
            break;
        } else {
            off += ret;
        }
    }
    printf("ogg done\n");

    l_samples = (uint16_t*)malloc(size / 2);
    r_samples = (uint16_t*)malloc(size / 2);
    for (int i = 0; i < size / 2; i++) {
        uint16_t sample = ((uint16_t*)samples)[i];
        if (i % 2) {
            r_samples[i/2] = sample;
        } else {
            l_samples[i/2] = sample;
        }
    }

    DCStoreRange(samples, size);
    DCStoreRange(l_samples, size / 2);
    DCStoreRange(r_samples, size / 2);

    printf("deinterleaved\n");

    free(samples);
    samples = NULL;

    for (uint i = 0; i < voices.size(); i++) {
        auto& v = voices[i];
        v = AXAcquireVoice(31, NULL, NULL);
        if (!v) {
            printf("Audio: couldn't get voice!\n");
            continue;
        }

        AXVoiceBegin(v);
        AXSetVoiceType(v, 0);

        AXSetVoiceVe(v, &vol);

        float srcRatio = SampleRate / AXGetInputSamplesPerSec();
        AXSetVoiceSrcRatio(v, srcRatio);
        AXSetVoiceSrcType(v, AX_VOICE_SRC_TYPE_LINEAR);

        AXSetVoiceDeviceMix(v, AX_DEVICE_TYPE_DRC, 0, stereo_mix[i]);
        AXSetVoiceDeviceMix(v, AX_DEVICE_TYPE_TV, 0, stereo_mix[i]);

        AXVoiceOffsets offs = {
            .dataType = SampleFormat,
            .loopingEnabled = AX_VOICE_LOOP_ENABLED,
            .loopOffset = 0,
            .endOffset = (uint32_t)(size / 4),
            .currentOffset = 0,
            .data = (i % 2) ? l_samples : r_samples,
        };

        AXSetVoiceOffsets(v, &offs);

        AXVoiceEnd(v);
    }
}

void PlayAudio() {
    for (auto& v : voices) {
        AXSetVoiceState(v, AX_VOICE_STATE_PLAYING);
    }
}

uint32_t GetTotalSamples() {
    AXVoiceOffsets offs;
    AXGetVoiceOffsets(voices[0], &offs);
    return offs.endOffset;
}

uint32_t GetCurrentSample() {
    AXVoiceOffsets offs;
    AXGetVoiceOffsets(voices[0], &offs);
    return offs.currentOffset;
}

}
