#pragma once

#include <array>
#include <sndcore2/voice.h>

#include "OggDecoder.h"

namespace Audio {

void InitAudio(OggDecoder& decoder);
void PlayAudio();
uint32_t GetTotalSamples();
uint32_t GetCurrentSample();

};
