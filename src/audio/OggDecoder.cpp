/***************************************************************************
 * Copyright (C) 2010
 * by Dimok
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any
 * damages arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any
 * purpose, including commercial applications, and to alter it and
 * redistribute it freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you
 * must not claim that you wrote the original software. If you use
 * this software in a product, an acknowledgment in the product
 * documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and
 * must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 * distribution.
 *
 * for WiiXplorer 2010
 ***************************************************************************/
/* This is an altered version of the original, available at
 * https://github.com/Maschell/libgui/blob/master/source/sounds/OggDecoder.cpp */
#include <unistd.h>
#include <malloc.h>
#include <cstdio>
#include <cerrno>
#include "OggDecoder.h"

OggDecoder::OggDecoder(const char * filepath) {
    this->stdio_file = fopen(filepath, "rb");
    if (this->stdio_file) {
        this->OpenFile();
    } else {
        printf("ogg open error %d\n", errno);
    }
}

OggDecoder::~OggDecoder()
{
    /*ExitRequested = true;
    while(Decoding)
        os_usleep(100);*/

    if(this->stdio_file)
        ov_clear(&ogg_file);
}

int64_t OggDecoder::Size() {
    return ov_pcm_total(&ogg_file, -1) * 2; //cuz 16bit samples
}

void OggDecoder::OpenFile()
{
    int ret;
    ret = ov_open(this->stdio_file, &ogg_file, NULL, 0);
    if (ret < 0) {
        fclose(this->stdio_file);
        this->stdio_file = NULL;
        return;
    }

    ogg_info = ov_info(&ogg_file, -1);
    if(!ogg_info) {
        ov_clear(&ogg_file);
        return;
    }
}

int OggDecoder::Rewind() {
    if(!this->stdio_file)
        return -1;

    int ret = ov_time_seek(&ogg_file, 0);

    return ret;
}

long OggDecoder::Read(void* buffer, size_t buffer_size) {
    if(!this->stdio_file)
        return -1;

    int bitstream = 0;

    long read = ov_read(&ogg_file, (char *) buffer, buffer_size, 1, 2, 1, &bitstream);

    return read;
}
