#include "Distort.hpp"
#include <whb/gfx.h>
#include <cstdio>
#include <glm/glm.hpp>

#include "RTT.hpp"
#include "shaders.h"
#include "gx2util.hpp"
#include "util.h"

Distort::Distort() : rtt() {
    if (!rtt.valid) return;

    if (!WHBGfxLoadGFDShaderGroup(&rtt.shader, 0, distort_shader)) {
        printf("Failed to parse texture shader!\n");
        return;
    }

    uint32_t buffer = 0;
    rtt.aPosition = buffer++;
    WHBGfxInitShaderAttribute(&rtt.shader,
        "aPosition", rtt.aPosition, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32);
    rtt.aTexCoord = buffer++;
    WHBGfxInitShaderAttribute(&rtt.shader,
        "aTexCoord", rtt.aTexCoord, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32);

    WHBGfxInitFetchShader(&rtt.shader);

    u_boffset = GX2FindPixelUniformReg(rtt.shader.pixelShader, "u_boffset");
    u_goffset = GX2FindPixelUniformReg(rtt.shader.pixelShader, "u_goffset");
    u_distort = GX2FindPixelUniformReg(rtt.shader.pixelShader, "u_distort");
    u_distortamp = GX2FindPixelUniformReg(rtt.shader.pixelShader, "u_distortamp");

    if (!u_boffset || !u_goffset || !u_distort || !u_distortamp) {
        return;
    }

    valid = true;
}

Distort::~Distort() {}

void Distort::Prepare() {
    rtt.Prepare();
}

void Distort::Draw() {
    rtt.SetShaders();

    GX2SetPixelUniformReg(
        u_boffset->offset, ARR_SIZE(boffset), (uint32_t*) &boffset
    );
    GX2SetPixelUniformReg(
        u_goffset->offset, ARR_SIZE(goffset), (uint32_t*) goffset
    );
    GX2SetPixelUniformReg(
        u_distort->offset, ARR_SIZE(distort), (uint32_t*) distort
    );
    GX2SetPixelUniformReg(
        u_distortamp->offset, ARR_SIZE(distortamp), (uint32_t*) distortamp
    );

    rtt.Draw();
}
