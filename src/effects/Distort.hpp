#pragma once
#include "RTT.hpp"
#include <glm/glm.hpp>
#include <glm/ext/scalar_constants.hpp>
#include <gx2/shaders.h>

/* In theory these constants are supposed to make for a single period, but
 * it turns out more like 6 */
constexpr float pi2() { return glm::pi<float>() * 2; }
constexpr float npi() { return -glm::pi<float>(); }

class Distort {
public:
    Distort();
    ~Distort();

    bool valid = false;

    void Prepare();
    void Draw();

    void distortAmplitude(float amp) { distortamp[0] = amp; }
    void distortParams(glm::vec2 s){ distort[0] = s.x; distort[1] = s.y; }
    void chromaGShift(glm::vec2 s) { goffset[0] = s.x; goffset[1] = s.y; }
    void chromaBShift(glm::vec2 s) { boffset[0] = s.x; boffset[1] = s.y; }

    Distort(Distort const &) = delete;
    void operator=(Distort const &x) = delete;

    RTT rtt;
private:
    GX2UniformVar *u_goffset, *u_boffset, *u_distortamp, *u_distort;
    float distortamp[1] = { 0.005f },
        goffset[2] = { -0.005f,  0.000f },
        boffset[2] = {  0.005f,  0.000f },
        distort[2] = { pi2(), npi() };
};
