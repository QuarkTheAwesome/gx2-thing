#include "NoEffect.hpp"
#include <whb/gfx.h>
#include <cstdio>
#include <glm/glm.hpp>

#include "RTT.hpp"
#include "shaders.h"
#include "util.h"
#include <glm/ext/scalar_constants.hpp>

NoEffect::NoEffect() : rtt() {
    if (!rtt.valid) return;

    if (!WHBGfxLoadGFDShaderGroup(&rtt.shader, 0, texture_shader)) {
        printf("Failed to parse texture shader!\n");
        return;
    }

    uint32_t buffer = 0;
    rtt.aPosition = buffer++;
    WHBGfxInitShaderAttribute(&rtt.shader,
        "aPosition", rtt.aPosition, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32);
    rtt.aTexCoord = buffer++;
    WHBGfxInitShaderAttribute(&rtt.shader,
        "aTexCoord", rtt.aTexCoord, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32);

    WHBGfxInitFetchShader(&rtt.shader);

    valid = true;
}

NoEffect::~NoEffect() {}

void NoEffect::Prepare() {
    rtt.Prepare();
}

void NoEffect::Draw() {
    rtt.SetShaders();
    rtt.Draw();
}
