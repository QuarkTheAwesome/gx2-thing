#pragma once
#include "RTT.hpp"

class NoEffect {
public:
    NoEffect();
    ~NoEffect();

    bool valid = false;

    void Prepare();
    void Draw();

    NoEffect(NoEffect const &) = delete;
    void operator=(NoEffect const &x) = delete;
private:
    RTT rtt;
};
