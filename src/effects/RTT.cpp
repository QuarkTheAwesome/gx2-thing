#include "RTT.hpp"
#include <stdbool.h>
#include <malloc.h>
#include <cstdio>
#include <iostream>
#include <exception>

#include <gx2/context.h>
#include <gx2/surface.h>
#include <gx2/event.h>
#include <gx2/state.h>
#include <gx2/shaders.h>
#include <gx2/texture.h>
#include <gx2/mem.h>
#include <gx2/sampler.h>
#include <gx2/draw.h>
#include <gx2/registers.h>
#include <gx2/clear.h>
#include <gx2r/surface.h>
#include <gx2r/buffer.h>
#include <gx2r/draw.h>
#include <whb/gfx.h>

#include <glm/glm.hpp>

#include "gx2util.hpp"
#include "util.h"
#include "shaders.h"

RTT::RTT() {
    ctx = (GX2ContextState*) memalign(
        GX2_CONTEXT_STATE_ALIGNMENT, sizeof(GX2ContextState)
    );
    if (!ctx) return;

    GX2InitSampler(&sampler,
        GX2_TEX_CLAMP_MODE_WRAP, GX2_TEX_XY_FILTER_MODE_POINT
    );

    memset(ctx, 0, sizeof(*ctx));
    GX2SetupContextStateEx(ctx, 0);
    GX2SetContextState(ctx);

/*  Some flags */
    GX2SetAlphaTest(TRUE, GX2_COMPARE_FUNC_GREATER, 0.0f);
    GX2SetDepthOnlyControl(TRUE, TRUE, GX2_COMPARE_FUNC_LESS);
    GX2SetCullOnlyControl(GX2_FRONT_FACE_CCW, FALSE, FALSE);
    GX2SetColorControl(GX2_LOGIC_OP_COPY, 0xFF, FALSE, TRUE);

    GX2CalcSurfaceSizeAndAlignment(&tex.surface);
    GX2InitTextureRegs(&tex);
    tex.surface.image = memalign(tex.surface.alignment, tex.surface.imageSize);
    if (!tex.surface.image) return;

    GX2CalcSurfaceSizeAndAlignment(&depth.surface);
    GX2InitDepthBufferRegs(&depth);
    depth.surface.image = memalign(depth.surface.alignment, depth.surface.imageSize);
    if (!depth.surface.image) return;

    GX2RCreateBuffer(&aPositionBuffer);
    GX2RCreateBuffer(&aTexCoordBuffer);
    ResetDrawCoords();

    valid = true;
}

RTT::~RTT() {
    GX2RDestroyBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
    GX2RDestroyBufferEx(&aTexCoordBuffer, (GX2RResourceFlags)0);
    if (tex.surface.image) free(tex.surface.image);
    if (depth.surface.image) free(depth.surface.image);
    if (ctx) free(ctx);
}

void RTT::ResetDrawCoords() {
        DrawCoords* aPositions = (DrawCoords*)GX2RLockBufferEx(
        &aPositionBuffer, (GX2RResourceFlags)0
    );
    DrawCoords* aTexCoords = (DrawCoords*)GX2RLockBufferEx(
        &aTexCoordBuffer, (GX2RResourceFlags)0
    );
    OnLeavingScope r([&] {
        GX2RUnlockBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
        GX2RUnlockBufferEx(&aTexCoordBuffer, (GX2RResourceFlags)0);
    });

#ifndef NDEBUG
    if (GX2RSizeof(aPositionBuffer) != sizeof(*aPositions) ||
        GX2RSizeof(aTexCoordBuffer) != sizeof(*aTexCoords)
    ) {
        std::cerr << __FILE__ << ":" << __LINE__ << ": GX2R buffer size mismatch: "
            << GX2RSizeof(aPositionBuffer) << " != " << sizeof(*aPositions) << " || "
            << GX2RSizeof(aTexCoordBuffer) << " != " << sizeof(*aTexCoords) << "\n";
        return;
    }
#endif

    *aPositions = (DrawCoords) { .coords = {
        [0] = glm::vec2(-1.0f,  1.0f),
        [1] = glm::vec2( 1.0f,  1.0f),
        [2] = glm::vec2( 1.0f, -1.0f),
        [3] = glm::vec2(-1.0f, -1.0f),
    }};
    *aTexCoords = (DrawCoords) { .coords = {
        [0] = glm::vec2( 0.0f,  0.0f),
        [1] = glm::vec2( 1.0f,  0.0f),
        [2] = glm::vec2( 1.0f,  1.0f),
        [3] = glm::vec2( 0.0f,  1.0f),
    }};
}
void RTT::SetDrawCoords(DrawCoords coords) {
    DrawCoords* aPositions = (DrawCoords*)GX2RLockBufferEx(
        &aPositionBuffer, (GX2RResourceFlags)0
    );
    OnLeavingScope r([&] {
        GX2RUnlockBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
    });

#ifndef NDEBUG
    if (GX2RSizeof(aPositionBuffer) != sizeof(*aPositions)) {
        std::cerr << __FILE__ << ":" << __LINE__ << ": GX2R buffer size mismatch: "
            << GX2RSizeof(aPositionBuffer) << " != " << sizeof(*aPositions) << "\n";
        return;
    }
#endif

    *aPositions = coords;
}

void RTT::Prepare() { //todo move into contextstate/constructor
    GX2ColorBuffer cbuf = (GX2ColorBuffer) {
        .surface = tex.surface,
        .viewNumSlices = 1,
    };

    GX2InitColorBufferRegs(&cbuf);

    GX2SetContextState(ctx);
    GX2SetColorBuffer(&cbuf, GX2_RENDER_TARGET_0);
    GX2SetDepthBuffer(&depth);

    GX2SetViewport(0, 0, RTT_WIDTH, RTT_HEIGHT, 0.0f, 1.0f);
    GX2SetScissor(0, 0, RTT_WIDTH, RTT_HEIGHT);

    GX2ClearColor(&cbuf, 0.0f, 0.0f, 0.0f, 0.0f);
    GX2ClearDepthStencilEx(&depth, depth.depthClear, depth.stencilClear,
        (GX2ClearFlags)(GX2_CLEAR_FLAGS_BOTH));
    GX2SetContextState(ctx);
}

void RTT::SetShaders() {
    GX2SetFetchShader(&shader.fetchShader);
    GX2SetVertexShader(shader.vertexShader);
    GX2SetPixelShader(shader.pixelShader);
}


void RTT::Draw() {
/*  Enable blending TODO: make this customisable per-effect */
    GX2SetColorControl(GX2_LOGIC_OP_COPY, 0xFF, FALSE, TRUE);
    GX2SetBlendControl(GX2_RENDER_TARGET_0,
    /*  RGB = [srcRGB * srcA] + [dstRGB * (1-srcA)] */
        GX2_BLEND_MODE_SRC_ALPHA, GX2_BLEND_MODE_INV_SRC_ALPHA,
        GX2_BLEND_COMBINE_MODE_ADD,
        TRUE,
    /*  A = [srcA * 1] + [dstA * (1-srcA)] */
        GX2_BLEND_MODE_ONE, GX2_BLEND_MODE_INV_SRC_ALPHA,
        GX2_BLEND_COMBINE_MODE_ADD);
/*  Disable depth test */
    GX2SetDepthOnlyControl(FALSE, FALSE, GX2_COMPARE_FUNC_ALWAYS);

    GX2SetPixelSampler(&sampler, 0);
    GX2SetPixelTexture(&tex, 0);

    GX2RSetAttributeBuffer(
        &aPositionBuffer, aPosition,
        aPositionBuffer.elemSize, 0
    );
    GX2RSetAttributeBuffer(
        &aTexCoordBuffer, aTexCoord,
        aTexCoordBuffer.elemSize, 0
    );

    GX2DrawEx(GX2_PRIMITIVE_MODE_QUADS,
        aPositionBuffer.elemCount, 0, 1);
}
