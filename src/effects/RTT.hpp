#pragma once
#include <gx2/context.h>
#include <gx2/surface.h>
#include <gx2/sampler.h>
#include <gx2/texture.h>
#include <gx2r/buffer.h>
#include <gx2r/resource.h>
#include <whb/gfx.h>

#include <glm/glm.hpp>

#include "util.h"

#define RTT_WIDTH 854
#define RTT_HEIGHT 480

class RTT {
public:
    RTT();
    ~RTT();

    bool valid = false;

    void Prepare();
    void SetShaders();
    void Draw();

    void SetDrawCoords(DrawCoords coords);
    void ResetDrawCoords();

    RTT(RTT const &) = delete;
    void operator=(RTT const &x) = delete;

    WHBGfxShaderGroup shader;
    uint32_t aPosition;
    uint32_t aTexCoord;
private:
    GX2RBuffer aPositionBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec2),
        .elemCount = 4,
    };
    GX2RBuffer aTexCoordBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec2),
        .elemCount = 4,
    };

    GX2Sampler sampler;
    GX2ContextState* ctx;
    GX2Texture tex = {
        .surface = (GX2Surface) {
            .dim = GX2_SURFACE_DIM_TEXTURE_2D,
            .width = RTT_WIDTH,
            .height = RTT_HEIGHT,
            .depth = 1,
            .mipLevels = 1,
            .format = GX2_SURFACE_FORMAT_UNORM_R8_G8_B8_A8,
            .use = (GX2SurfaceUse)(GX2_SURFACE_USE_TEXTURE | GX2_SURFACE_USE_COLOR_BUFFER),
            .tileMode = GX2_TILE_MODE_DEFAULT,
        },
        .viewNumSlices = 1,
        .compMap = 0x00010203,
    };
    GX2DepthBuffer depth = {
        .surface = (GX2Surface) {
            .dim = GX2_SURFACE_DIM_TEXTURE_2D,
            .width = RTT_WIDTH,
            .height = RTT_HEIGHT,
            .depth = 1,
            .mipLevels = 1,
            .format = GX2_SURFACE_FORMAT_FLOAT_R32,
            .use = (GX2SurfaceUse)(GX2_SURFACE_USE_DEPTH_BUFFER | GX2_SURFACE_USE_TEXTURE),
            .tileMode = GX2_TILE_MODE_DEFAULT,
        },
        .viewNumSlices = 1,
        .depthClear = 1.0f,
    };
};
