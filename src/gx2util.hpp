#pragma once
#include <cstring>
#include <cstdlib>
#include <string>

#define GX2SetVertexUniformRegGLM(offset, value) { \
    GX2SetVertexUniformReg(offset, \
        sizeof(value) / sizeof(float), \
        (uint32_t*)glm::value_ptr(value) \
    ); \
}

static inline GX2UniformVar* GX2FindUniformReg(GX2VertexShader* shader, std::string name) {
    for (size_t i = 0; i < shader->uniformVarCount; i++) {
        if (name == shader->uniformVars[i].name) {
            return &shader->uniformVars[i];
        }
    }
    return NULL;
}

static inline GX2UniformVar* GX2FindPixelUniformReg(GX2PixelShader* shader, std::string name) {
    for (size_t i = 0; i < shader->uniformVarCount; i++) {
        if (name == shader->uniformVars[i].name) {
            return &shader->uniformVars[i];
        }
    }
    return NULL;
}

#define GX2RSizeof(buffer) (buffer.elemSize * buffer.elemCount)
