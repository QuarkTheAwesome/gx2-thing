#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <iostream>
#include <fstream>

#include <whb/proc.h>
#include <whb/log.h>
#include <whb/gfx.h>
#include <gx2r/buffer.h>
#include <gx2r/draw.h>
#include <gx2r/resource.h>
#include <gx2/draw.h>
#include <vpad/input.h>
#include <coreinit/time.h>
#include <coreinit/cache.h>

#include <romfs-wiiu.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "ext/zstr.hpp"

#include "main.h"
#include "log.h"
#include "util.h"
#include "gx2util.hpp"
#include "shaders.h"
#include "Texture.hpp"
#include "obj/Model.hpp"
#include "effects/Distort.hpp"
#include "effects/NoEffect.hpp"
#include "state/AppState.hpp"
#include "audio/Audio.hpp"
#include "audio/OggDecoder.h"

#ifdef NDEBUG
#pragma message("This is a release build!")
#endif

static WHBGfxShaderGroup shader;

int main(int argc, char** argv) {
    LOGInit();
    WHBProcInit();
    WHBGfxInit();
    ramfsInit();
/*  Make sure to clean everything up later */
    OnLeavingScope r([&] {
        printf("done\n");

        ramfsExit();
        WHBGfxShutdown();
        WHBProcShutdown();
        LOGShutdown();
    });

    printf("hey\n");

    { //scope so ogg goes away faster
        OggDecoder ogg("resin:/music.ogg");
        int ret = ogg.Rewind();
        if (ret < -1) {
            printf("Couldn't open music!\n");
            return 1;
        }

        Audio::InitAudio(ogg);
    }

/*  Gotta fill out the values in this thing */
    GfxState gfx;

    if (!WHBGfxLoadGFDShaderGroup(&shader, 0, main_shader)) {
        printf("Failed to parse shader!\n");
        return 1;
    }

/*  This buffer count gets used later for GX2RSetAttributeBuffer */
    uint32_t buffer = 0;
    gfx.aPosition = buffer++;
    WHBGfxInitShaderAttribute(&shader,
        "aPosition", gfx.aPosition, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32_32_32);
    gfx.aColour = buffer++;
    WHBGfxInitShaderAttribute(&shader,
        "aColour", gfx.aColour, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32_32);
    gfx.aVertexNormal = buffer++;
    WHBGfxInitShaderAttribute(&shader,
        "aVertexNormal", gfx.aVertexNormal, 0, GX2_ATTRIB_FORMAT_FLOAT_32_32_32_32);

    WHBGfxInitFetchShader(&shader);

    gfx.uMVP = GX2FindUniformReg(
        shader.vertexShader, "uMVP"
    );
    gfx.uLightModelPosition = GX2FindUniformReg(
        shader.vertexShader, "uLightModelPosition"
    );
    if (!gfx.uMVP || !gfx.uLightModelPosition) {
        printf("couldn't find shader uniforms!\n");
        return 1;
    }

    gfx.ProjectionMatrix = glm::perspective(
        glm::radians(30.0f),
        16.0f / 9.0f,
        0.1f, 100.0f
    );

    if (!gfx.check()) {
        printf("bug: GfxState fails self-test!\n");
        return 1;
    }

/*  Load tv intro texture */
    Texture tvIntro(1280, 720);
    if (!tvIntro.valid) {
        printf("bug: tvIntro fails self-test!\n");
        return 1;
    }
    zstr::ifstream tvIntroTex("resin:/tv.z", std::ios::binary);
    tvIntro.Load(tvIntroTex);

/*  Deliberately Deterministic */
    std::mt19937 rd(0xCAFE);
/*  Only x_dist, y_dist and z_dist matter after init */
    std::uniform_real_distribution<> x_dist(-2.0f,  2.0f);
    std::uniform_real_distribution<> y_init_dist(-3.0f, 3.0f);
    std::uniform_real_distribution<> y_dist( 3.0f,  5.0f);
    std::uniform_real_distribution<> z_dist(-2.0f, 2.0f);
    std::uniform_real_distribution<> a_dist( 0.0f,  glm::pi<float>() / 2);

    std::vector<Model> objects(30, Model("resin:/object.obj"));
    for (auto& object : objects) {
        if (!object.valid) {
            printf("couldn't load object.obj!\n");
            return 1;
        }
        object.position = (glm::vec3) {
            .x =  x_dist(rd),
            .y =  y_init_dist(rd),
            .z =  z_dist(rd),
        };
        object.angle = (glm::vec3) {
            .x =  a_dist(rd),
            .y =  a_dist(rd),
            .z =  a_dist(rd),
        };
        object.scale = 0.5;
    }

    //TODO: proper MVP math
    #define SCREEN_HEIGHT 3.0f
    //units/sec
    #define Y_SPEED -1.0f
    //radians/sec
    #define Y_ANGLE_SPEED (glm::pi<float>() / 4)
    #define X_ANGLE_SPEED (-glm::pi<float>() / 4)
    VPADVec2D lastLStickInput;
    VPADVec2D lastRStickInput;

    Audio::PlayAudio();

    OSTick lastTick = OSGetTick();

    printf("having a go\n");

    while (WHBProcIsRunning()) {
        WHBGfxBeginRender();
        AppState state = GetAppState();

        if (gfx.black) {
            WHBGfxBeginRenderDRC();
            WHBGfxClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            WHBGfxFinishRenderDRC();
        } else if (state == INTRO_TV || state == DONUTS) {
        /*  First: draw the donuts to donut.pass */
            //Rotate: Camera spin
            /*gfx.ViewMatrix = glm::rotate(glm::mat4(1.0f),
                0.0f, glm::vec3(0, 1, 0)
            );*/
            //Translate: Camera position
            /*gfx.ViewMatrix = glm::translate(gfx.ViewMatrix,
                gfx.camPosition
            );*/
            gfx.ViewMatrix = glm::lookAt(
                gfx.camPosition,
                glm::vec3(0.0f, 0.0f,  0.0f),
                glm::vec3(0.0f, 1.0f,  0.0f));

            gfx.donut.pass.Prepare();

            GX2SetFetchShader(&shader.fetchShader);
            GX2SetVertexShader(shader.vertexShader);
            GX2SetPixelShader(shader.pixelShader);

            for (auto& object: objects) {
                object.Draw(gfx);
            }

            if (state == INTRO_TV) {
                glm::vec2 c(0.05f, 0.2f);
                //TODO do zoom2d stuff for tvIntro.pass instead
                float size = 0.40f + gfx.zoom2d;
                gfx.donut.pass.rtt.SetDrawCoords((DrawCoords) { .coords = {
                    [0] = glm::vec2(-size,  size) + c,
                    [1] = glm::vec2( size,  size) + c,
                    [2] = glm::vec2( size, -size) + c,
                    [3] = glm::vec2(-size, -size) + c,
                }});
                size = 1.0f + gfx.zoom2d;
                tvIntro.SetDrawCoords((DrawCoords) { .coords = {
                    [0] = glm::vec2(-size,  size),
                    [1] = glm::vec2( size,  size),
                    [2] = glm::vec2( size, -size),
                    [3] = glm::vec2(-size, -size),
                }});

                /*TODO gfx.tvIntro.pass.Prepare();
                tvIntro.Draw();
                gfx.donut.pass.Draw();*/

                WHBGfxBeginRenderDRC();
                WHBGfxClearColor(0.0f, 0.4f, 0.4f, 1.0f);
                //gfx.tvIntro.pass.Draw();
                tvIntro.Draw();
                gfx.donut.pass.Draw();
                WHBGfxFinishRenderDRC();
            } else /* DONUTS */ {
            /*  Just draw fullscreen, right to the screens */
                gfx.donut.pass.rtt.ResetDrawCoords();

                WHBGfxBeginRenderDRC();
                WHBGfxClearColor(0.0f, 0.4f, 0.4f, 1.0f);
                gfx.donut.pass.Draw();
                WHBGfxFinishRenderDRC();
            }
        } else {
            printf("bug: unknown appstate!\n");
        }



/*        WHBGfxBeginRenderTV();
        WHBGfxClearColor(1.0f, 0.4f, 0.4f, 1.0f);

        //renderPass1.Draw();

        WHBGfxFinishRenderTV();*/
        WHBGfxFinishRender();

        OSTick curTick = OSGetTick();
        uint32_t delta = (uint32_t)OSTicksToMilliseconds(curTick - lastTick);
        float delta_s = (float)delta / 1000.0f;

        lastTick = curTick;

        VPADStatus status;
        VPADReadError err;
        VPADRead(VPAD_CHAN_0, &status, 1, &err);
        if (err == VPAD_READ_SUCCESS) {
            lastLStickInput = status.leftStick;
            lastRStickInput = status.rightStick;
        }

        for (auto& object : objects) {
            object.angle.x += delta_s * X_ANGLE_SPEED;
            object.angle.y += delta_s * Y_ANGLE_SPEED;
            object.position.y += delta_s * Y_SPEED;
            if (object.position.y < -SCREEN_HEIGHT) {
                object.position = (glm::vec3) {
                    .x = x_dist(rd),
                    .y = y_dist(rd),
                    .z = z_dist(rd),
                };
            }
        }

        RunAppLogic(gfx);

    /*  Run all the animations */
        gfx.donut.params.animate(delta_s);

    /*  Apply the new values */
        gfx.donut.params.applyTo(gfx.donut.pass);
        //printf("camx: %0.2f\n", gfx.camPosition.x);
    }
    return 0;
}

#ifndef NDEBUG
extern "C" {
    void __wrap_abort() {
        WHBLogPrint("Abort called! Forcing stack trace.\n");
        *(unsigned int*)(0xDEADC0DE) = 0xBABECAFE;
        for(;;) {}
    }
}
#endif
