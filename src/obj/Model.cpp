#include "Model.hpp"
#include <stdlib.h>
#include <stdint.h>

#include <gx2/shaders.h>
#include <gx2r/draw.h>
#include <gx2/draw.h>
#include <gx2r/buffer.h>
#include <gx2r/resource.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "tiny_obj_loader.h"
#include "gx2util.hpp"
#include "util.h"
#include "GfxState.hpp"

void Model::Draw(const GfxState& gfx) {
    if (!gfx.check() || !valid) return;

    //Rotate: model itself
    glm::mat4 ModelMatrix = glm::translate(glm::mat4(1.0f),
        position
    );
    ModelMatrix = glm::rotate(ModelMatrix,
        angle.x, glm::vec3(1, 0, 0)
    );
    ModelMatrix = glm::rotate(ModelMatrix,
        angle.y, glm::vec3(0, 1, 0)
    );
    ModelMatrix = glm::rotate(ModelMatrix,
        angle.z, glm::vec3(0, 0, 1)
    );
    //Pre-calculate this for later BEFORE scaling
    glm::mat4 ModelMatrixInv = glm::inverse(ModelMatrix);
    //Finally, scale
    ModelMatrix = glm::scale(ModelMatrix,
        glm::vec3(scale)
    );

    glm::mat4 MVPMatrix = gfx.ProjectionMatrix * gfx.ViewMatrix * ModelMatrix;

    //Calculate light position in modelspace
    glm::vec4 LightModelPosition = ModelMatrixInv * gfx.LightPosition;

    GX2SetVertexUniformRegGLM(gfx.uMVP->offset, MVPMatrix);
    GX2SetVertexUniformRegGLM(gfx.uLightModelPosition->offset, LightModelPosition);

    GX2RSetAttributeBuffer(
        &getPositionBuffer(), gfx.aPosition,
        getPositionBuffer().elemSize, 0
    );
    GX2RSetAttributeBuffer(
        &getColourBuffer(), gfx.aColour,
        getColourBuffer().elemSize, 0
    );
    GX2RSetAttributeBuffer(
        &getVertexNormalBuffer(), gfx.aVertexNormal,
        getVertexNormalBuffer().elemSize, 0
    );
    GX2DrawEx(GX2_PRIMITIVE_MODE_TRIANGLES,
        getPositionBuffer().elemCount, 0, 1
    );
}

Model::Model(std::string path) {
    modelReader.ParseFromFile(path);
    if (!modelReader.Valid()) return;

/*  Only parsing shape 0 for now */
    tinyobj::shape_t shape = modelReader.GetShapes()[0];
    const tinyobj::attrib_t& attrib = modelReader.GetAttrib();

/*  Get total vertex count for this shape */
    size_t total_vertex_count = 0;
    for (size_t face = 0; face < shape.mesh.num_face_vertices.size(); face++) {
        size_t vertex_count = shape.mesh.num_face_vertices[face];
        total_vertex_count += vertex_count;
    }

/*  Allocate our attribute buffers */
    aPositionBuffer.elemCount = total_vertex_count;
    aColourBuffer.elemCount = aPositionBuffer.elemCount;
    aVertexNormalBuffer.elemCount = aPositionBuffer.elemCount;
    GX2RCreateBuffer(&aPositionBuffer);
    GX2RCreateBuffer(&aColourBuffer);
    GX2RCreateBuffer(&aVertexNormalBuffer);

/*  Lock them now - ready to be filled in */
    glm::vec4* aPositions = (glm::vec4*)GX2RLockBufferEx(
        &aPositionBuffer, (GX2RResourceFlags)0);
    glm::vec4* aVertexNormals = (glm::vec4*)GX2RLockBufferEx(
        &aVertexNormalBuffer, (GX2RResourceFlags)0);
    glm::vec3* aColours = (glm::vec3*)GX2RLockBufferEx(
        &aColourBuffer, (GX2RResourceFlags)0);
    OnLeavingScope r([&] {
        GX2RUnlockBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
        GX2RUnlockBufferEx(&aVertexNormalBuffer, (GX2RResourceFlags)0);
        GX2RUnlockBufferEx(&aColourBuffer, (GX2RResourceFlags)0);
    });

    glm::vec3 materialColour(0.75f, 0.0f, 1.0f);
    if (modelReader.GetMaterials().size() > 0) {
        materialColour = glm::make_vec3(modelReader.GetMaterials()[0].diffuse);
    }


/*  We're going to de-indexify this model and make a flat buffer of attributes. */
    size_t out_vertex = 0;
/*  Each face might have a different number of vertexes, so we'll use an offset
    variable to index into mesh.indices instead of the usual i*size approach */
    size_t face_offset = 0;
    for (size_t face = 0; face < shape.mesh.num_face_vertices.size(); face++) {
        size_t vertex_count = shape.mesh.num_face_vertices[face];

        for (size_t vertex = 0; vertex < vertex_count; vertex++) {
            tinyobj::index_t indexes = shape.mesh.indices[face_offset + vertex];
            size_t vertex_idx = 3 * indexes.vertex_index;
            size_t normal_idx = 3 * indexes.normal_index;

            aPositions[out_vertex].x = attrib.vertices[vertex_idx + 0];
            aPositions[out_vertex].y = attrib.vertices[vertex_idx + 1];
            aPositions[out_vertex].z = attrib.vertices[vertex_idx + 2];
            aPositions[out_vertex].w = 1.0f;
            aVertexNormals[out_vertex].x = attrib.normals[normal_idx + 0];
            aVertexNormals[out_vertex].y = attrib.normals[normal_idx + 1];
            aVertexNormals[out_vertex].z = attrib.normals[normal_idx + 2];
            aVertexNormals[out_vertex].w = 1.0f;

            aColours[out_vertex] = materialColour;
            out_vertex++;
        }
        face_offset += vertex_count;
    }

    valid = true;
}

Model::Model(Model const& from) :
    position(from.position), angle(from.angle), scale(from.scale), valid(from.valid) {
    aPositionBuffer.elemSize = from.aPositionBuffer.elemSize;
    aPositionBuffer.elemCount = from.aPositionBuffer.elemCount;
    aVertexNormalBuffer.elemSize = from.aVertexNormalBuffer.elemSize;
    aVertexNormalBuffer.elemCount = from.aVertexNormalBuffer.elemCount;
    aColourBuffer.elemSize = from.aColourBuffer.elemSize;
    aColourBuffer.elemCount = from.aColourBuffer.elemCount;

    GX2RCreateBuffer(&aPositionBuffer);
    GX2RCreateBuffer(&aColourBuffer);
    GX2RCreateBuffer(&aVertexNormalBuffer);

/*  Lock our new buffers */
    glm::vec4* aPositions = (glm::vec4*)GX2RLockBufferEx(
        &aPositionBuffer, (GX2RResourceFlags)0);
    glm::vec4* aVertexNormals = (glm::vec4*)GX2RLockBufferEx(
        &aVertexNormalBuffer, (GX2RResourceFlags)0);
    glm::vec3* aColours = (glm::vec3*)GX2RLockBufferEx(
        &aColourBuffer, (GX2RResourceFlags)0);
    OnLeavingScope r([&] {
        GX2RUnlockBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
        GX2RUnlockBufferEx(&aVertexNormalBuffer, (GX2RResourceFlags)0);
        GX2RUnlockBufferEx(&aColourBuffer, (GX2RResourceFlags)0);
    });

/*  HACKY: using GX2RBuffer.buffer directly. from is const, makes things hard */
    glm::vec4* from_aPositions = (glm::vec4*)from.aPositionBuffer.buffer;
    glm::vec4* from_aVertexNormals = (glm::vec4*)from.aVertexNormalBuffer.buffer;
    glm::vec4* from_aColours = (glm::vec4*)from.aColourBuffer.buffer;

/*  Copy data */
    memcpy(aPositions, from_aPositions, GX2RSizeof(aPositionBuffer));
    memcpy(aVertexNormals, from_aVertexNormals, GX2RSizeof(aVertexNormalBuffer));
    memcpy(aColours, from_aColours, GX2RSizeof(aColourBuffer));
}

Model::~Model() {
    GX2RDestroyBufferEx(&aPositionBuffer, (GX2RResourceFlags)0);
    GX2RDestroyBufferEx(&aVertexNormalBuffer, (GX2RResourceFlags)0);
    GX2RDestroyBufferEx(&aColourBuffer, (GX2RResourceFlags)0);
}
