#pragma once
#include <stdbool.h>

#include <gx2r/buffer.h>

#include <glm/glm.hpp>

#include "tiny_obj_loader.h"
#include "GfxState.hpp"

class Model {
public:
    Model(std::string path);
    ~Model();
    void Draw(const GfxState& gfx);

    glm::vec3 position;
    glm::vec3 angle;
    float scale = 1.0f;
    bool valid = false;

    GX2RBuffer& getPositionBuffer() { return aPositionBuffer; }
    GX2RBuffer& getVertexNormalBuffer() { return aVertexNormalBuffer; }
    GX2RBuffer& getColourBuffer() { return aColourBuffer; }

    Model(Model const &);
    void operator=(Model const &x) = delete;
private:
    tinyobj::ObjReader modelReader;

    GX2RBuffer aPositionBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec4),
    };
    GX2RBuffer aVertexNormalBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec4),
    };
    GX2RBuffer aColourBuffer = (GX2RBuffer) {
        .flags = (GX2RResourceFlags)(GX2R_RESOURCE_BIND_VERTEX_BUFFER |
                                    GX2R_RESOURCE_USAGE_CPU_READ |
                                    GX2R_RESOURCE_USAGE_CPU_WRITE |
                                    GX2R_RESOURCE_USAGE_GPU_READ),
        .elemSize = sizeof(glm::vec3),
    };
};
