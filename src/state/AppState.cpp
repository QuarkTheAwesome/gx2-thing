#include "AppState.hpp"
#include "GfxState.hpp"
#include "util.h"

#include <glm/glm.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include <tuple>
#include <string>

#include <coreinit/time.h>
#include "audio/Audio.hpp"

enum Command {
    CAMERA_MOVE,
    STOP_CAMERA,
    FAKE_ZOOM, //only if implemented by main()!
    SET_DISTORT_AMP,
    SET_DISTORT_PARAMS,
    SET_CHROMA_G,
    SET_CHROMA_B,
    BLACK_SCREEN,
    UNBLACK_SCREEN,
    NEXT_APPSTATE,
    DO_NOTHING,
    LOOP,
};
std::string command_texts[] = {
    "CAMERA_MOVE",
    "STOP_CAMERA",
    "FAKE_ZOOM", //only if implemented by main()!
    "SET_DISTORT_AMP",
    "SET_DISTORT_PARAMS",
    "SET_CHROMA_G",
    "SET_CHROMA_B",
    "BLACK_SCREEN",
    "UNBLACK_SCREEN",
    "NEXT_APPSTATE",
    "DO_NOTHING",
    "LOOP",
};
// This command processor is kinda buggy, so there's some odd filler in here. LOOP is on the chopping block.
/*static const std::vector<std::tuple<Command, float, int, std::vector<float>, std::vector<float>>> commands = {
    { DO_NOTHING,         0.0f,  0, {}, {} },
//Reset some basic parameters
    { SET_DISTORT_PARAMS, 0.0f,  0, { pi2(), npi() }, { 0.0f, 0.0f } },
    { STOP_CAMERA,        0.0f,  0, {}, {} },
    { FAKE_ZOOM,          0.0f,  0, { 0.0f }, { 0.0f } },
//2s INTRO_TV. start with no distortion or anything.
    { SET_DISTORT_AMP,    0.0f,  0, { 0.000f }, { 0.000f } },
    { SET_CHROMA_G,       0.0f,  0, { -0.000f, 0.000f }, { -0.000f, 0.000f } },
    { SET_CHROMA_B,       1.8f,  0, {  0.000f, 0.000f }, {  0.000f, 0.000f } },
//3.5s wait. Start pulsing chromatic aberration.
    { SET_CHROMA_G,       0.0f,  0, { -0.000f, 0.000f }, { -0.075f, 0.000f } },
    { SET_CHROMA_B,       0.9f,  0, {  0.000f, 0.000f }, {  0.075f, 0.000f } },
    { SET_CHROMA_G,       0.0f,  0, { -0.000f, 0.000f }, { -0.075f, 0.000f } },
    { SET_CHROMA_B,       0.9f,  0, {  0.000f, 0.000f }, {  0.075f, 0.000f } },
    { SET_CHROMA_G,       0.0f,  0, { -0.000f, 0.000f }, { -0.075f, 0.000f } },
    { SET_CHROMA_B,       0.9f,  0, {  0.000f, 0.000f }, {  0.075f, 0.000f } },
    { SET_CHROMA_G,       0.0f,  0, { -0.000f, 0.000f }, { -0.075f, 0.000f } },
    { SET_CHROMA_B,       0.9f,  0, {  0.000f, 0.000f }, {  0.075f, 0.000f } },
//1.8s zoom! 1u/s for 1s -> zoom=1u -> ""2x"" zoom
    { FAKE_ZOOM,          1.1f,  0, { 0.0f }, { 1.0f } },
    { SET_DISTORT_AMP,    0.6f,  0, { 0.000f }, { 1.000f } },
    { BLACK_SCREEN,       0.1f,  0, {}, {} },
//DONUTS. fullscreen donuts fall
    { NEXT_APPSTATE,      0.0f,  0, {}, {} },
    { FAKE_ZOOM,          0.0f,  0, { 0.0f }, { 0.0f } },
    { SET_DISTORT_AMP,    0.0f,  0, { 0.005f }, { 0.000f } },
//flickering? kinda neat
    { UNBLACK_SCREEN,     0.2f,  0, {}, {} },
    { BLACK_SCREEN,       0.1f,  0, {}, {} },
    { UNBLACK_SCREEN,     0.0f,  0, {}, {} },

    { DO_NOTHING,         8.0f,  0, {}, {} },
    { BLACK_SCREEN,       0.5f,  0, {}, {} },
    { UNBLACK_SCREEN,     0.0f,  0, {}, {} },

    { SET_DISTORT_AMP,    0.0f,  0, { 0.005f }, { -0.005f / 20 }},
    { NEXT_CAMERA_MOVE,  20.0f,  0, {  0.0f,  0.0f, -3.0f }, {  0.0000f,  0.0010f, -1.0100f }},
    { NEXT_CAMERA_MOVE,   0.0f,  0, {  0.0f,  0.0f, -3.0f }, {  0.0000f,  0.0000f,  0.0000f }},

    { LOOP,               0.0f,  0, {}, {} },
};*/

typedef struct CommandData {
    Command cmd;
    uint32_t start_sample;
    uint32_t length_samples;
    std::vector<float> args;
} CommandData;

static const std::vector<CommandData> commands_new = {
    { DO_NOTHING,           0,     0, {} },
    { FAKE_ZOOM,       259645, 16400, { 0.0f, 0.6f } },
    { FAKE_ZOOM,       267045,     0, { 0.0f, 0.0f } },
    { FAKE_ZOOM,       281186, 16488, { 0.0f, 0.6f } },
    { FAKE_ZOOM,       297675,     0, { 0.0f, 0.0f } },
    { FAKE_ZOOM,       303070, 16344, { 0.0f, 0.6f } },
    { FAKE_ZOOM,       319415,     0, { 0.0f, 0.0f } },
    { FAKE_ZOOM,       324822, 21430, { 0.0f, 2.0f } },
//    { BLACK_SCREEN,    340911,  5341, {} },
    { NEXT_APPSTATE,   340911,     0, {} },
    { FAKE_ZOOM,       340911,     0, { 0.0f, 0.0f } },
    { CAMERA_MOVE,     340911, 300000, {
         0.0f, 0.0f, -6.0f,
        20.0f, 0.0f,  0.0f,
         0.0f, 0.0f,  6.0f,
    } },
    { CAMERA_MOVE,     640911, 300000, {
          0.0f, 0.0f,  6.0f,
        -20.0f, 0.0f,  0.0f,
          0.0f, 0.0f, -6.0f,
    } },
    { DO_NOTHING,    UINT_MAX,     0, {} },
};
static auto current_command = commands_new.begin();
static std::vector<CommandData> running_commands;

static void RunCommand(GfxState& gfx, CommandData cmd, float t);

static void NextAppState();
static AppState state = INTRO_TV;

void RunAppLogic(GfxState& gfx) {
    uint32_t now_sample = Audio::GetCurrentSample();
    float demo_percent = (float)now_sample / (float)Audio::GetTotalSamples();

/*  Delete anything where "the part you care about" returns true. filter()? */
    running_commands.erase(std::remove_if(
        running_commands.begin(), running_commands.end(),
    /*  the part you care about: */
        [&] (CommandData c) {
            if (c.start_sample + c.length_samples < now_sample) {
                RunCommand(gfx, c, 1.0f);
                return true;
            } else return false;
        }
    ), running_commands.end());

/*  If the next command is due to start... TODO make a loop */
    if (current_command->start_sample <= now_sample) {
    /*  If it has length, put it in the ongoing commands array */
        if (current_command->length_samples) {
            printf("enqueuing %s\n", command_texts[current_command->cmd].c_str());
            running_commands.push_back(*current_command);
    /*  Otherwise, just run it now */
        } else {
            puts("oneshot: ");
            RunCommand(gfx, *current_command, 1.0f);
        }
    /*  As long as we have a LOOP this is always safe */
        std::advance(current_command, 1);
    }

    for (auto cmd : running_commands) {
        uint32_t t_samples = now_sample - cmd.start_sample;
        float t = (float)t_samples / cmd.length_samples;
        if (t > 1.0f) t = 1.0f;
        RunCommand(gfx, cmd, t);
    }
}

static void RunCommand(GfxState& gfx, CommandData cmd, float t) {
    printf("running %s, %0.2f\n", command_texts[cmd.cmd].c_str(), t);

    switch(cmd.cmd) {
        case CAMERA_MOVE: {
#ifndef NDEBUG
            if (cmd.args.size() != 9) {
                puts("Invalid arguments to CAMERA_MOVE command!\n");
                break;
            }
#endif
            glm::vec3 n1(cmd.args[0], cmd.args[1], cmd.args[2]);
            glm::vec3 n2(cmd.args[3], cmd.args[4], cmd.args[5]);
            glm::vec3 n3(cmd.args[6], cmd.args[7], cmd.args[8]);
            glm::vec3 l1 = glm::mix(n1, n2, t);
            glm::vec3 l2 = glm::mix(n2, n3, t);
            gfx.camPosition = glm::mix(l1, l2, t);
            break;
        }
        case FAKE_ZOOM: {
#ifndef NDEBUG
            if (cmd.args.size() != 2) {
                puts("Invalid arguments to FAKE_ZOOM command!\n");
                break;
            }
#endif
            gfx.zoom2d = cmd.args[1] * t; //HACK
            break;
        }
        case NEXT_APPSTATE: {
            NextAppState();
            break;
        }
        case BLACK_SCREEN: {
            if (t == 1.0f) {
                gfx.black = false;
            } else {
                gfx.black = true;
            }
            break;
        }
        case LOOP: {
            current_command = commands_new.begin();
            state = INTRO_TV; //hack
            break;
        }
        default:
#ifndef NDEBUG
        {
            std::string err;
            if (cmd.cmd >= 0 && cmd.cmd < LOOP) {
                err = command_texts[cmd.cmd];
            } else err = "unk";
            printf("WARN: unimplemented command %d! %s\n", cmd.cmd, err.c_str());
            break;
        }
#endif
        case DO_NOTHING: {
            break;
        }
    }
}

//static AppState state = INTRO_TV;
AppState GetAppState() {
    return state;
}

static void NextAppState() {
    switch (state) {
        case INTRO_TV: state = DONUTS; break;
        case DONUTS:   state = OUTRO_TV; break;
        case OUTRO_TV: state = EXIT; break;
        default: break;
    }
}
