#pragma once
#include "GfxState.hpp"

enum AppState {
    INTRO_TV,
    DONUTS,
    //todo: montage
    OUTRO_TV,
    EXIT,
};

void RunAppLogic(GfxState& gfx);
AppState GetAppState();
